# pdftools

`pdfstamp`: stamp a pdf with some text  
`pdfsign`: sign a pdf using a certificate from the nssdb

## Requirements

### pdfstamp

- enscript
- ps2pdf (ghostscript)
- pdftk

### pdfsign

- pdfsig
- gpg
- nssdb in .pki/nssdb with client certificate(s)
