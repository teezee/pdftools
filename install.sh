#!/bin/bash

pkg="pdftools.d"
instdir="${HOME}/.local/bin/$pkg"

declare -a files=( "pdfsign" "pdfstamp" )

for f in "${files[@]}"; do
    install -v -D "$f" "${instdir}/$f";
done

